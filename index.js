const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://lhemarcanete:admin123@224canete.vd2heps.mongodb.net/s35?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"));

db.once("open", ()=> console.log(`Connected to MongoDB`));

const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model("User", userSchema);

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.post("/signup", (req,res)=> {

    User.findOne({username: req.body.username}, (err, result)=>{

        if(result != null && result.username == req.body.username){
            return res.send("User currently in used")
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });

            newUser.save((saveErr, savedUser)=> {
                if(saveErr){
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New user registered");
                }
            })
        }
    })
});

app.listen(port, ()=> console.log(`Server running at port ${port}`));